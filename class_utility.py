#!/usr/bin/env python

# Copyright 2022 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys, os

## Get the #define guard name from the filename, with pre guard name prepended.
def get_header_guard(file_name, pre_guard = "CPPOCL_GUARD_"):
    guard = file_name.replace(".", "_")
    guard = guard.replace("/", "_")
    guard = guard.replace("\\", "_")
    return pre_guard + guard.upper()

## Read a file and return the text lines.
def read_file(file_name):
    lines = []

    try:
        f = open(file_name, "r")
    except:
        return lines

    line = " "
    while len(line) > 0:
        line = f.readline()
        if line:
            lines.append(line)

    return lines

# Create a file containing the text lines.
def create_file(full_path, lines):
    if not os.path.exists(full_path):
        f = None
        try:
            f = open(full_path, "w+")
        except:
            return False

        for line in lines:
            f.write("{}\n".format(line))

        f.close()
    else:
        return False

    return True

## Get the lines for a class including the #define guard block.
def get_class_header_lines(class_name,
                           define_guard,
                           nocopy, nomove,
                           virtual_dtor = True,
                           space_count = 4,
                           base_class = "",
                           member_functions = None):

    spaces = " " * space_count
    dtor = "virtual ~" + class_name if virtual_dtor else "~" + class_name
    copy_funcs = "delete" if nocopy else "default"
    move_funcs = "delete" if nomove else "default"

    if base_class:
        base_class = " : public " + base_class

    lines = [
        "#ifndef " + define_guard,
        "#define " + define_guard,
        "",
        "class " + class_name + base_class,
        "{",
        "public:",
        spaces + class_name + "() = default;",
        spaces + dtor + "() = default;",
        spaces + class_name + "(" + class_name + " const&) = " + copy_funcs + ";",
        spaces + class_name + "(" + class_name + "&&) = " + move_funcs + ";",
        spaces + class_name + "& " + class_name + "(" + class_name + " const&) = " + copy_funcs + ";",
        spaces + class_name + "& " + class_name + "(" + class_name + "&&) = " + move_funcs + ";",
        "};",
        "",
        "#endif // " + define_guard
    ]

    return lines
