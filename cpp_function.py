#!/usr/bin/env python

# Copyright 2022 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from doxygen import Doxygen

class CppFunction:

    # Dictionary of default values for common C++ types.
    # Used for the implementation part of the .cpp file.
    DEFAULT_RETURN = {
        "void"               : "",
        "void*"              : "nullptr",
        "void *"             : "nullptr",
        "bool"               : "false",
        "char"               : r"'\0'",
        "signed char"        : "0",
        "unsigned char"      : "0U",
        "unsigned char*"     : "nullptr",
        "unsigned char *"    : "nullptr",
        "char*"              : "nullptr",
        "char *"             : "nullptr",
        "const char*"        : "nullptr",
        "char const*"        : "nullptr",
        "const char *"       : "nullptr",
        "char const *"       : "nullptr",
        "short"              : "0",
        "signed short"       : "0",
        "unsigned short"     : "0U",
        "int"                : "0",
        "signed int"         : "0",
        "unsigned int"       : "0U",
        "long"               : "0",
        "signed long"        : "0",
        "unsigned long"      : "0U",
        "long long"          : "0",
        "signed long long"   : "0",
        "unsigned long long" : "0U",
        "int8_t"             : "0",
        "uint8_t"            : "0U",
        "std::int8_t"        : "0",
        "std::uint8_t"       : "0U",
        "int16_t"            : "0",
        "uint16_t"           : "0U",
        "std::int16_t"       : "0",
        "std::uint16_t"      : "0U",
        "int32_t"            : "0",
        "uint32_t"           : "0U",
        "std::int32_t"       : "0",
        "std::uint32_t"      : "0U",
        "int64_t"            : "0",
        "uint64_t"           : "0U",
        "std::int64_t"       : "0",
        "std::uint64_t"      : "0U",
        "float"              : "0.0f",
        "double"             : "0.0",
        "string"             : "\"\"",
        "std::string"        : "\"\"",
        }

    ## Initialise members for creating a function for a header or implementation file.
    def __init__(self,
                 class_name,
                 function_name,
                 return_type,
                 args,
                 is_default,
                 is_deleted,
                 is_const,
                 is_abstract,
                 indent_spaces = 4,
                 indent_tabs = 0,
                 comments = None,
                 doxygen = None):
        self.class_name    = class_name
        self.function_name = function_name
        self.return_type   = return_type
        self.args          = args
        self.is_default    = is_default
        self.is_deleted    = is_deleted
        self.is_const      = is_const
        self.is_abstract   = is_abstract
        self.indent_spaces = indent_spaces
        self.indent_tabs   = indent_tabs
        self.comments      = CppFunction.GetAsList(comments)
        self.doxygen       = doxygen if doxygen else Doxygen()

    ## Get the function signature and comments for a header file, as a list of strings.
    def GetHeader(self):
        return_type = self.return_type + " " if self.return_type else ""
        signature   = return_type + self.function_name + "(" + ", ".join(self.args) + ")"
        if self.is_default:
            signature += " default"
        elif self.is_deleted:
            signature += " delete"
        if self.is_const:
            signature += " const"
        if self.is_abstract:
            signature += " = 0"
        signature += ";"

        lines = self.doxygen.get_comments(self.comments)
        lines += [signature]
        return lines

    ## Get the function signature for a .cpp file with empty implementation, as a list of strings.
    def GetImplementation(self):
        indent = " " * self.indent_spaces
        indent +=  "\t" * self.indent_tabs
        return_type = self.return_type + " " if self.return_type else ""
        class_name  = self.class_name + "::" if self.class_name else ""
        signature   = return_type + class_name + self.function_name + "(" + ", ".join(self.args) + ")"
        if self.is_const:
            signature += " const"
        default_value = CppFunction.DEFAULT_RETURN.get(self.return_type)
        return_str = indent + "return " + default_value + ";" if default_value else ""

        lines = [signature]
        lines += ["{", return_str, "}"] if return_str else ["{", "}"]
        return lines

    ## Get the value as a list, either as a list containing a string, or [] when value is None.
    @staticmethod
    def GetAsList(value):
        if type(value) is str:
            l = [value]
        elif value:
            l = value
        else:
            l = []
        return l
