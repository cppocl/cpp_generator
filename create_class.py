#!/usr/bin/env python

# Copyright 2022 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys, os
from datetime import datetime
import class_utility

def create_class_file(class_name,
                      path,
                      file_name,
                      copyright,
                      pre_guard = "CPPOCL_GUARD_",
                      nocopy = False,
                      nomove = False,
                      novdtor = False,
                      spaces = 4,
                      base_class = "",
                      member_functions = None):

    full_file_path = os.path.join(path, file_name)
    guard = class_utility.get_header_guard(full_file_path, pre_guard)

    lines = []
    lines += copyright
    lines.append("")
    lines += class_utility.get_class_header_lines(class_name,
                                                  guard,
                                                  nocopy,
                                                  nomove,
                                                  not novdtor,
                                                  spaces,
                                                  base_class,
                                                  member_functions)

    if class_utility.create_file(full_file_path, lines):
        print("Created file " + full_file_path)
    else:
        sys.stderr.write("Unable to create file: " + full_file_path + "\n")

# Standard copyright for CPPOCL projects.
copyright = (
    "/*",
    "Copyright {} Colin Girling".format(datetime.now().year),
    "",
    "Licensed under the Apache License, Version 2.0 (the \"License\");",
    "you may not use this file except in compliance with the License.",
    "You may obtain a copy of the License at",
    "",
    "    http://www.apache.org/licenses/LICENSE-2.0",
    "",
    "Unless required by applicable law or agreed to in writing, software",
    "distributed under the License is distributed on an \"AS IS\" BASIS,",
    "WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.",
    "See the License for the specific language governing permissions and",
    "limitations under the License.",
    "*/",
    "")

# Command line options:
ARG_COPYRIGHT = "--copyright" # Specify copyright input filename.
ARG_SPACES    = "--spaces"    # Number of space characters for indentation.
ARG_PREGUARD  = "--preguard"  # String prepended to the #ifndef #define guard.
ARG_BASECLASS = "--baseclass" # Specify the base class name.
ARG_NOCOPY    = "--nocopy"    # No copy constructor and copy assignment operator.
ARG_NOMOVE    = "--nomove"    # No move constructor and move assignment operator.
ARG_NODTOR    = "--novdtor"   # No virtual destructor.

ARG_COPYRIGHT_DESCRIPTION = "Specify your own copyright text file"
ARG_SPACES_DESCRIPTION    = "Number of space characters for indentation"
ARG_PREGUARD_DESCRIPTION  = "String prepended to the #ifndef #define guard"
ARG_NOCOPY_DESCRIPTION    = "Set copy constructor and copy assignment operator to deleted"
ARG_NOMOVE_DESCRIPTION    = "Set move constructor and move assignment operator to deleted"
ARG_NODTOR_DESCRIPTION    = "Destructor of class is not virtual"

args = [ARG_COPYRIGHT, ARG_SPACES, ARG_PREGUARD, ARG_BASECLASS, ARG_NOCOPY, ARG_NOMOVE, ARG_NODTOR]

args_descriptions = [
    ARG_COPYRIGHT_DESCRIPTION, ARG_SPACES_DESCRIPTION, ARG_PREGUARD_DESCRIPTION,
    ARG_NOCOPY_DESCRIPTION, ARG_NOMOVE_DESCRIPTION, ARG_NODTOR_DESCRIPTION]

def print_help():
    args_help = ""
    for arg in args:
        args_help += "[" + arg + "] "

    help = ["create_class.py <class name> [filename] [path] " + args_help,
            "",
            "class name    Name of the class.",
            "filename      Optional name of the header file, otherwise the class name + .py is used.",
            "path          Optional name of the target path to create the file.",
            ""]

    for n, desc in enumerate(args_descriptions):
        pad = " " * (14 - len(args[n]))
        help.append(args[n]  + pad + desc)

    help.append("")
    help += [
        "Examples:",
        "",
        "create_class.py TestClass",
        "create_class.py --copyright=copyright_test_file.txt --spaces=2 --baseclass=Base --nocopy --nomove --novdtor TestClass TestClass.hpp SubFolder"]

    for help_line in help:
        print(help_line)

if __name__ == "__main__":

    class_name = ""
    base_class = ""
    file_name  = ""
    path       = ""
    spaces     = 4
    pre_guard  = "CPPOCL_GUARD_"
    nocopy     = False
    nomove     = False
    novdtor    = False
    member_functions = []

    for arg in sys.argv[1:]:
        if arg.startswith("--"):
            if arg.startswith(ARG_COPYRIGHT + "="):
                filename = arg[len(ARG_COPYRIGHT) + 1:]
                c = class_utility.read_file(filename)
                if c:
                    copyright = c
            elif arg.startswith(ARG_SPACES + "="):
                spaces = int(arg[len(ARG_SPACES) + 1:])
            elif arg.startswith(ARG_PREGUARD + "="):
                pre_guard = arg[len(ARG_PREGUARD) + 1:]
            elif arg.startswith(ARG_BASECLASS + "="):
                base_class = arg[len(ARG_BASECLASS) + 1:]
            elif arg == ARG_NOCOPY:
                nocopy = True
            elif arg == ARG_NOMOVE:
                nomove = True
            elif arg == "--help":
                print_help()
                sys.exit(0)
        elif not class_name:
            class_name = arg
        elif not file_name:
            file_name = arg
        elif not path:
            path = arg

    if class_name:
        if not file_name:
            file_name = class_name + ".hpp"

        create_class_file(class_name,
                          path,
                          file_name,
                          copyright,
                          pre_guard,
                          nocopy,
                          nomove,
                          novdtor,
                          spaces,
                          base_class,
                          member_functions)
    else:
        print("Class name expected")
