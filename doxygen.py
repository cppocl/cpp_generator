#!/usr/bin/env python

# Copyright 2022 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

class Doxygen:

    STYLE_JAVADOCS = 1 # Using /**  */ style
    STYLE_QT       = 2 # Using /*! */ style
    STYLE_CPP1     = 3 # Using /// style
    STYLE_CPP2     = 4 # Using //! style

    ## Select the style and if there is an asterisk for each line.
    ## NOTE: Asterisk is always ignored for STYLE_CPP1
    def __init__(self, style = STYLE_CPP1, asterisk = False):
        self._style = style
        self._asterisk = asterisk
        self._first_row = ""
        self._row_start = ""
        self._last_row = ""
        self.set_style(style, asterisk)

    ## Set the style and asterisk for each comment line.
    def set_style(self, style, asterisk = False):
        self._style = style
        self._asterisk = asterisk

        if (self._style == Doxygen.STYLE_JAVADOCS):
            self._first_row = "/**"
            self._row_start = " * " if self._asterisk else " "
            self._last_row  = " */"
        elif (self._style == Doxygen.STYLE_QT):
            self._first_row = "/*!"
            self._row_start = " * " if self._asterisk else " "
            self._last_row  = " */"
        elif (self._style == Doxygen.STYLE_CPP1):
            self._row_start = "/// "
        elif (self._style == Doxygen.STYLE_CPP2):
            self._row_start = "//! "

    ## Create the comment rows from the text rows.
    def get_comments(self, text_rows):
        comments = []

        if text_rows and self._first_row:
            comments.append(self._first_row)

        for r in text_rows:
            comments.append(self._row_start + r)

        if text_rows and self._last_row:
            comments.append(self._last_row)

        return comments

    @staticmethod
    def get_style_as_string(style):
        if style == Doxygen.STYLE_JAVADOCS: return "JavaDocs"
        if style == Doxygen.STYLE_QT: return "Qt"
        if style == Doxygen.STYLE_CPP1: return "CPP1"
        if style == Doxygen.STYLE_CPP2: return "CPP2"
