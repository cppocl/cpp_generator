#!/usr/bin/env python

# Copyright 2022 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Disable the generation of byte code files to prevent unit tests failing.
import sys
sys.dont_write_bytecode = True

from doxygen import Doxygen
from cpp_function import CppFunction

import unittest

def FindInList(string_list, sub_string):
    for index, s in enumerate(string_list):
        if s.find(sub_string) != -1:
            return index
    return -1

class TestDoxygen(unittest.TestCase):

    def test_Doxygen(self):

        help_text = "Help for function"

        doxygen_styles = [Doxygen.STYLE_JAVADOCS, Doxygen.STYLE_QT, Doxygen.STYLE_CPP1, Doxygen.STYLE_CPP2]
        asterisk = [True, False]

        doxygen_results_asterisk = {
            Doxygen.STYLE_JAVADOCS : ["/**", f" * {help_text}", " */"],
            Doxygen.STYLE_QT       : ["/*!", f" * {help_text}", " */"],
            Doxygen.STYLE_CPP1     : [f"/// {help_text}"],
            Doxygen.STYLE_CPP2     : [f"//! {help_text}"]
        }

        doxygen_results_no_asterisk = {
            Doxygen.STYLE_JAVADOCS : ["/**", f" {help_text}", " */"],
            Doxygen.STYLE_QT       : ["/*!", f" {help_text}", " */"],
            Doxygen.STYLE_CPP1     : [f"/// {help_text}"],
            Doxygen.STYLE_CPP2     : [f"//! {help_text}"]
        }

        for style in doxygen_styles:
            for a in asterisk:
                doxygen_results = doxygen_results_asterisk if a else doxygen_results_no_asterisk
                d = Doxygen(style, a)
                comments = d.get_comments([help_text])
                results  = doxygen_results[style]
                self.assertTrue(comments == results)

class TestCppFunction(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_Function(self):

        # Test free functions.
        cpp_function = CppFunction("", "func", "void", [], False, False, False, False)
        hpp_lines = cpp_function.GetHeader()
        cpp_lines = cpp_function.GetImplementation()
        self.assertTrue(hpp_lines == ["void func();"])
        self.assertTrue(cpp_lines == ["void func()", "{", "}"])

        cpp_function = CppFunction("", "func", "int", ["int a"], False, False, False, False)
        hpp_lines = cpp_function.GetHeader()
        cpp_lines = cpp_function.GetImplementation()
        self.assertTrue(hpp_lines == ["int func(int a);"])
        self.assertTrue(cpp_lines == ["int func(int a)", "{", "    return 0;", "}"])

        cpp_function = CppFunction("", "func", "char", ["int a", "char b"], False, False, False, False)
        hpp_lines = cpp_function.GetHeader()
        cpp_lines = cpp_function.GetImplementation()
        self.assertTrue(hpp_lines == ["char func(int a, char b);"])
        self.assertTrue(cpp_lines == ["char func(int a, char b)", "{", r"    return '\0';", "}"])

        cpp_function = CppFunction("", "func", "char", ["int a", "char b"],
                                   False, False, False, False, 4, 0,
                                   ["func with int and char", "a an int", "b a char"])
        hpp_lines = cpp_function.GetHeader()
        cpp_lines = cpp_function.GetImplementation()
        self.assertTrue(hpp_lines == ["/// func with int and char", "/// a an int", "/// b a char", "char func(int a, char b);"])
        self.assertTrue(cpp_lines == ["char func(int a, char b)", "{", r"    return '\0';", "}"])

        # Test member functions of a class.
        cpp_function = CppFunction("Test", "func", "void", [], False, False, False, False)
        hpp_lines = cpp_function.GetHeader()
        cpp_lines = cpp_function.GetImplementation()
        self.assertTrue(hpp_lines == ["void func();"])
        self.assertTrue(cpp_lines == ["void Test::func()", "{", "}"])

        cpp_function = CppFunction("Test", "func", "int", ["int a"], False, False, False, False)
        hpp_lines = cpp_function.GetHeader()
        cpp_lines = cpp_function.GetImplementation()
        self.assertTrue(hpp_lines == ["int func(int a);"])
        self.assertTrue(cpp_lines == ["int Test::func(int a)", "{", "    return 0;", "}"])

        cpp_function = CppFunction("Test", "func", "char", ["int a", "char b"], False, False, False, False)
        hpp_lines = cpp_function.GetHeader()
        cpp_lines = cpp_function.GetImplementation()
        self.assertTrue(hpp_lines == ["char func(int a, char b);"])
        self.assertTrue(cpp_lines == ["char Test::func(int a, char b)", "{", r"    return '\0';", "}"])

if __name__ == '__main__':
    unittest.main()
